import { useSelector } from "react-redux";
import { PlaceLocator, selectIsPlaceUnknown } from "./places";
import { Weather } from "./weather";

export const App = () => {
  const isPlaceUnknown = useSelector(selectIsPlaceUnknown);
  return isPlaceUnknown ? <PlaceLocator /> : <Weather />;
};
