import { configureStore } from "@reduxjs/toolkit";
import { placesSlice } from "./places";

export const store = configureStore({
  reducer: {
    places: placesSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
