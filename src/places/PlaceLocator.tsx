import { Typography, Button } from "antd";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Page, usePosition } from "../shared";
import { Place } from "./places.models";
import { placesSlice } from "./places.slice";

const { addNewPlace } = placesSlice.actions;

const { Title, Text } = Typography;

export const PlaceLocator = () => {
  const [position, retry] = usePosition();
  const dispatch = useDispatch();

  const isLoading = position.status === "loading";
  const isPermissionDenied =
    position.status === "error" &&
    position.error.code === GeolocationPositionError.PERMISSION_DENIED;

  useEffect(() => {
    if (position.status === "finished") {
      dispatch(addNewPlace(Place.from(position.current.coords)));
    }
  }, [position, dispatch]);

  return (
    <Page>
      <div
        style={{
          height: "100%",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Title level={1}>Получаем прогноз погоды</Title>
        {isLoading && (
          <Text>Приложению нужен доступ к данным о вашем местоположении</Text>
        )}
        {isPermissionDenied && (
          <>
            <Text>
              Разрешите доступ к своему местоположению и нажмите кнопку ниже
            </Text>
            <div style={{ marginTop: 24 }}>
              <Button onClick={retry}>Попробовать снова</Button>
            </div>
          </>
        )}
      </div>
    </Page>
  );
};
