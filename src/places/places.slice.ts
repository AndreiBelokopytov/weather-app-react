import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Place } from "./places.models";
import { initialState, PlacesState } from "./places.state";

export const placesSlice = createSlice({
  name: "places",
  initialState,
  reducers: {
    addNewPlace(state: PlacesState, action: PayloadAction<Place>) {
      state.entities.push(action.payload);
    },
  },
});
