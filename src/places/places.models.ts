export type Location = {
  latitude: string;
  longitude: string;
};

export type Place = {
  location: Location;
  name: string;
};

const DEFAULT_NAME = "Новое место";

export const Place = {
  from(
    coordinates: GeolocationCoordinates,
    name: string = DEFAULT_NAME
  ): Place {
    return {
      location: {
        latitude: coordinates.latitude.toString(),
        longitude: coordinates.longitude.toString(),
      },
      name,
    };
  },
};
