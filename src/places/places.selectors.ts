import { RootState } from "../store";

export const selectIsPlaceUnknown = (state: RootState) => {
  return state.places.entities.length === 0;
};
