import { Place } from "./places.models";

export type PlacesState = {
  entities: Place[];
};

export const initialState: PlacesState = { entities: [] };
