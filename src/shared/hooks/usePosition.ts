import { useCallback, useEffect, useState } from "react";

type PositionState =
  | {
      status: "loading";
    }
  | {
      status: "finished";
      current: GeolocationPosition;
    }
  | {
      status: "error";
      error: GeolocationPositionError;
    };

export const usePosition = (): [PositionState, () => void] => {
  const [state, setState] = useState<PositionState>({ status: "loading" });
  const [retryCount, setRetryCount] = useState(0);

  const retry = useCallback(() => setRetryCount(retryCount + 1), [retryCount]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        setState({
          status: "finished",
          current: position,
        });
      },
      (error) => {
        setState({
          status: "error",
          error,
        });
      }
    );
  }, [retryCount]);

  return [state, retry];
};
