import { Layout } from "antd";

export const Page = ({ children }: React.PropsWithChildren) => (
  <Layout style={{ height: "100vh", padding: "24px" }}>{children}</Layout>
);
