import { Typography, Card } from "antd";
import { Page } from "../shared";

const { Title, Text } = Typography;

export const Weather = () => {
  return (
    <Page>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          marginBottom: "48px",
        }}
      >
        <Title level={2}>Санкт-Петербург, 18:24</Title>
        <Text type="secondary">59.937500, 30.308611</Text>
        <Title level={1}>-3° C</Title>
        <Title level={3}>пасмурно</Title>
      </div>
      <div style={{ flex: 1 }}>
        <div
          style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}
        >
          <Card style={{ flexBasis: "100%" }} title="Влажность"></Card>
          <Card
            style={{ flexBasis: "100%" }}
            title="Атмосферное давление"
          ></Card>
          <Card style={{ flexBasis: "100%" }} title="Ощущуается как"></Card>
          <Card style={{ flexBasis: "100%" }} title="Ветер">
            <Text>Западный, 5 м/c</Text>
          </Card>
        </div>
      </div>
    </Page>
  );
};
